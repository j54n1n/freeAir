---
title: "freeAir"
date: 2020-07-15T00:00:00-00:00
copyright: "Julian Sanin"
description: "A convenience Android TV app for bluMartin freeAir remote control service"

menu:
    - {url: "https://startbootstrap.com/themes/grayscale/", name: "Original"}
---
