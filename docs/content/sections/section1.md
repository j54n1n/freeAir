---
title: "About"
weight: 2
---

## About freeAir

Lorem ipsum freeAir. Simply download the template from [the releases page](https://gitlab.com/j54n1n/freeAir/-/releases). The app is open source, and you can use it for any purpose, personal or commercial.

This app was adapted from from the web service, brought to you by [bluMartin](https://blumartin.de/)

This app features stock resources from [freeAir Connect](https://freeair-connect.de/) along with customizations for [Google Android TV](https://www.android.com/tv/).
