---
title: "Links"
weight: 3
improvecontrast: true
---

## Download freeAir

You can download Grayscale for free from the releases page.

{{< big-button text="Visit Download Page" href="https://gitlab.com/j54n1n/freeAir/-/releases" >}}
