---
title: "Contact"
weight: 4
---

## Contact

Feel free to leave a comment on the [issues page](https://gitlab.com/j54n1n/freeAir/-/issues) to give some feedback about this app!
