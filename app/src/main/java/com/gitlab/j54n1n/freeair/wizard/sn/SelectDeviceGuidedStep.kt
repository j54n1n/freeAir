package com.gitlab.j54n1n.freeair.wizard.sn

import com.gitlab.j54n1n.freeair.R
import com.gitlab.j54n1n.freeair.wizard.Preferences

class SelectDeviceGuidedStep : SerialNumbersGuidedStep() {

    override fun getBreadcrumb(): String? {
        return activity?.getString(R.string.wizard_breadcrumb_selection_step)
    }

    override fun getDescription(): String? {
        return activity?.getString(R.string.wizard_serial_number_selection_desc)
    }

    override fun getButtonActionId(): Long {
        // Do not show a button in the 3rd row and make serial numbers non editable.
        return ACTION_ID_HIDDEN
    }

    override fun onDoConfirmSettings(id: Long): Boolean {
        // Never confirm settings in this guided step.
        return false
    }

    override fun shouldLaunchWebActivity(): Boolean {
        // There is only one device serial number. Therefore launch web activity directly.
        return (Preferences.getDeviceCount(activity) == 1L)
    }
}