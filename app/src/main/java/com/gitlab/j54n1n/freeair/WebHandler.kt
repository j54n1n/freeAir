package com.gitlab.j54n1n.freeair

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import android.widget.ProgressBar
import androidx.annotation.RequiresApi

/**
 * Web application handler for navigation and loading of URLs.
 */
abstract class WebHandler(
    private val webView: WebView, private val progressBar: ProgressBar,
    private val reloadButton: View
) {

    companion object {
        private const val BLANK_PAGE = "about:blank"
    }

    /**
     * Option to allow loading of a given URL or not.
     * @param url the given URL to be loaded.
     * @return true if the URL should be refused otherwise false.
     */
    abstract fun abortUrlLoading(url: String): Boolean

    /**
     * Handle web application loading errors.
     * @param errorCode the loading error code.
     * @param description the loading error reason.
     * @param failingUrl the URL that caused the loading error.
     */
    abstract fun failedToLoad(errorCode: Int?, description: String?, failingUrl: String?)

    /**
     * Execute additional DOM modifications via JavaScript execution when the page is loaded.
     * @return the JavaScript source code string to be executed.
     */
    abstract fun onPageLoadedExecuteJavaScript(): String

    /**
     * Visualize a reload button when the failing URL corresponds to the target URL.
     * @param failingUrl the URL that failed to load.
     * @param targetUrl the target URL that should be loaded.
     */
    fun tryAllowReload(failingUrl: String?, targetUrl: String) {
        if ((failingUrl != null) && failingUrl.startsWith(targetUrl)) {
            webView.loadUrl(BLANK_PAGE);
            reloadButton.visibility = View.VISIBLE
            reloadButton.requestFocus()
        }
    }

    /**
     * Reload target URL and hide reload button.
     * @param targetUrl the target URL that should be loaded.
     */
    fun reload(targetUrl: String) {
        webView.loadUrl(targetUrl)
        reloadButton.visibility = View.GONE
    }

    /**
     * Attempt backtracking of the navigation history.
     * @param keyCode if given KEYCODE_BACK the backtracking is attempted
     * @return true if backtracking has been done else false
     */
    fun tryGoBack(keyCode: Int): Boolean {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack() && quirkGoBack()) {
            webView.goBack()
            return true
        }
        return false
    }

    private fun quirkGoBack(): Boolean {
        // Quirk when URL differs only by an # anchor
        val urlHistory = webView.copyBackForwardList()
        if (urlHistory.currentIndex > 0) {
            var currentUrl = urlHistory.getItemAtIndex(urlHistory.currentIndex).url
            var previousUrl = urlHistory.getItemAtIndex(urlHistory.currentIndex - 1).url
            // Normalize before # anchor
            currentUrl = currentUrl.substringBefore('#')
            previousUrl = previousUrl.substringBefore('#')
            // Page is treated as same therefore do not navigate.
            if (currentUrl == previousUrl) {
                return false
            }
            // Page failed miserably therefore do not navigate.
            if ((previousUrl == BLANK_PAGE) || (currentUrl == BLANK_PAGE)) {
                return false
            }
        }
        return true
    }

    init {
        // Setup the hosting web view.
        webView.setBackgroundColor(Color.TRANSPARENT)
        webView.settings.apply {
            @SuppressLint("SetJavaScriptEnabled")
            javaScriptEnabled = true
            domStorageEnabled = true
            cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
            // Harden security settings.
            allowContentAccess = false
            allowFileAccess = false
            allowUniversalAccessFromFileURLs = false
            allowFileAccessFromFileURLs = false
        }
        webView.webViewClient = WebViewClientHandler()
        webView.webChromeClient = WebChromeClientHandler()
    }

    private inner class WebViewClientHandler : WebViewClient() {

        override fun shouldOverrideUrlLoading(
            view: WebView?, request: WebResourceRequest?
        ): Boolean {
            val url = request?.url.toString()
            return handleUrlLoading(url)
        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return handleUrlLoading(url.toString())
        }

        private fun handleUrlLoading(url: String): Boolean {
            return abortUrlLoading(url)
        }

        @RequiresApi(Build.VERSION_CODES.M)
        override fun onReceivedError(
            view: WebView?, request: WebResourceRequest?, error: WebResourceError?
        ) {
            handleOnReceivedError(
                error?.errorCode, error?.description.toString(), request?.url.toString())
        }

        override fun onReceivedError(
            view: WebView?, errorCode: Int, description: String?, failingUrl: String?
        ) {
            handleOnReceivedError(errorCode, description, failingUrl)
        }

        private fun handleOnReceivedError(
            errorCode: Int?, description: String?, failingUrl: String?
        ) {
            failedToLoad(errorCode, description, failingUrl)
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            progressBar.visibility = View.VISIBLE
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            progressBar.visibility = View.GONE
            quirkOnPageLoaded()
        }

        private fun quirkOnPageLoaded() {
            val script = onPageLoadedExecuteJavaScript()
            webView.evaluateJavascript(script, null)
        }
    }

    private inner class WebChromeClientHandler : WebChromeClient() {

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            progressBar.progress = newProgress
        }
    }
}