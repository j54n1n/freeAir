package com.gitlab.j54n1n.freeair

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.leanback.app.GuidedStepSupportFragment
import com.gitlab.j54n1n.freeair.wizard.DeviceCountGuidedStep
import com.gitlab.j54n1n.freeair.wizard.Preferences
import com.gitlab.j54n1n.freeair.wizard.sn.EnterSerialNumbersGuidedStep
import com.gitlab.j54n1n.freeair.wizard.sn.SelectDeviceGuidedStep

class MainActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val guidedStep = if (!Preferences.hasDeviceCount(this)) {
            DeviceCountGuidedStep()
        } else if (!Preferences.hasConfirmedSettings(this)
            || !Preferences.hasAllSerialNumbers(this)) {
            EnterSerialNumbersGuidedStep()
        } else {
            SelectDeviceGuidedStep()
        }
        GuidedStepSupportFragment.addAsRoot(this, guidedStep, android.R.id.content)
    }
}
