package com.gitlab.j54n1n.freeair.wizard.sn

import androidx.leanback.widget.GuidedAction
import com.gitlab.j54n1n.freeair.R
import com.gitlab.j54n1n.freeair.wizard.Preferences

/**
 * Guided step to insert serial numbers for the first time.
 */
class EnterSerialNumbersGuidedStep : SerialNumbersGuidedStep() {

    override fun getBreadcrumb(): String? {
        return activity?.getString(R.string.wizard_breadcrumb_final_step)
    }

    override fun getDescription(): String? {
        return resources.getQuantityString(R.plurals.wizard_serial_number_desc,
            Preferences.getDeviceCount(activity).toInt()
        )
    }

    override fun getButtonActionId(): Long {
        return GuidedAction.ACTION_ID_FINISH
    }

    override fun onDoConfirmSettings(id: Long): Boolean {
        // Confirm settings in this guided step when the finish button is pressed.
        return (GuidedAction.ACTION_ID_FINISH == id)
    }
}