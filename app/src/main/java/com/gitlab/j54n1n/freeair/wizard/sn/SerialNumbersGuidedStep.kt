package com.gitlab.j54n1n.freeair.wizard.sn

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import androidx.leanback.app.GuidedStepSupportFragment
import androidx.leanback.widget.GuidanceStylist
import androidx.leanback.widget.GuidedAction
import com.gitlab.j54n1n.freeair.R
import com.gitlab.j54n1n.freeair.WebActivity
import com.gitlab.j54n1n.freeair.wizard.Preferences
import java.lang.Long.parseLong

/**
 * Guided step with serial number input fields and an optional button action.
 */
abstract class SerialNumbersGuidedStep : GuidedStepSupportFragment() {

    companion object {
        /** Special Action ID which hides the button in the 3rd row. */
        const val ACTION_ID_HIDDEN = Preferences.MAX_DEVICE_COUNT + 1L
    }

    /**
     * Retrieve the breadcrumb string for the given guided step.
     * @return a string describing the breadcrumb title.
     */
    abstract fun getBreadcrumb(): String?

    /**
     * Retrieve the description string for the given guided step.
     * @return a string with detailed description of the given guided step.
     */
    abstract fun getDescription(): String?

    /**
     * Retrieve the button action ID of the given guided step.
     * @return one of the button action IDs such as ACTION_ID_CONTINUE or ACTION_ID_FINISH.
     * @see GuidedAction.ACTION_ID_CONTINUE
     * @see GuidedAction.ACTION_ID_FINISH
     */
    abstract fun getButtonActionId(): Long

    /**
     * Determine if the saved serial number should be confirmed.
     * After this calling further guided steps should not be necessary anymore,
     * except if more than one device is available.
     * @return true if setting can be confirmed otherwise false
     */
    abstract fun onDoConfirmSettings(id: Long): Boolean

    /**
     * Determine if the web activity should be launched directly.
     * @return true if the launch of the web activity is required otherwise false.
     */
    protected open fun shouldLaunchWebActivity(): Boolean {
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (shouldLaunchWebActivity()) {
            // There is only one device so launch it directly with id 1 and close hosting activity.
            launchWebActivity(activity, 1L)
            activity?.finishAfterTransition()
        }
    }

    /**
     * Launches the web activity with given selected saved serial number from a guided step.
     * @param activity the hosting activity of the guided steps.
     */
    private fun launchWebActivity(activity: Activity?, id: Long) {
        val serialNumber = Preferences.getSerialNumber(activity, id)
        val intent = Intent(activity?.applicationContext, WebActivity::class.java)
        WebActivity.putDeviceSN(intent, serialNumber)
        activity?.startActivity(intent)
    }

    override fun onCreateGuidance(savedInstanceState: Bundle?): GuidanceStylist.Guidance {
        val title = activity?.getString(R.string.app_name)
        return GuidanceStylist.Guidance(title, getDescription(), getBreadcrumb(), null)
    }

    private fun getSerialNumberAction(id: Long) : GuidedAction {
        val hasSerialNumber = Preferences.hasSerialNumber(activity, id)
        val savedSerialNumber = Preferences.getSerialNumber(activity, id).toString()
        val editTitle = if (!hasSerialNumber) "" else savedSerialNumber
        val description = if (hasSerialNumber) savedSerialNumber
        else getString(R.string.wizard_action_serial_number_example)
        val serialNumberNumeral =
            if (Preferences.getDeviceCount(activity) == 1L) "" else id.toString()
        val isEditable = (getButtonActionId() != ACTION_ID_HIDDEN)
        return GuidedAction.Builder(activity)
            .id(id)
            .title(getString(R.string.wizard_action_serial_number_title, serialNumberNumeral))
            .editTitle(editTitle)
            .description(description)
            .editDescription(getString(R.string.wizard_action_serial_number_title, serialNumberNumeral))
            .editInputType(InputType.TYPE_CLASS_NUMBER)
            .editable(isEditable)
            .build()
    }

    override fun onCreateActions(actions: MutableList<GuidedAction>, savedInstanceState: Bundle?) {
        for (id in Preferences.getDeviceCountRange(activity)) {
            actions.add(getSerialNumberAction(id))
        }
    }

    override fun onCreateButtonActions(
        actions: MutableList<GuidedAction>, savedInstanceState: Bundle?
    ) {
        if (getButtonActionId() != ACTION_ID_HIDDEN) {
            actions.add(GuidedAction.Builder(activity).clickAction(getButtonActionId()).build())
            actions[actions.size - 1].isEnabled = Preferences.hasAllSerialNumbers(activity)
        }
    }

    override fun onGuidedActionEditedAndProceed(action: GuidedAction?): Long {
        if (action?.id != null) {
            val serialNumber = action.editTitle
            val isValidNumber = (TextUtils.isDigitsOnly(serialNumber) && (serialNumber.length > 1))
            if (isValidNumber) {
                action.description = serialNumber
                return GuidedAction.ACTION_ID_NEXT
            } else if (serialNumber.isEmpty()) {
                action.description = activity?.getString(R.string.wizard_action_serial_number_example)
                return GuidedAction.ACTION_ID_CURRENT
            } else {
                action.description = activity?.getString(R.string.error_serial_number)
                return GuidedAction.ACTION_ID_CURRENT
            }
        }
        return GuidedAction.ACTION_ID_CURRENT
    }

    private fun updateButtonAction() {
        if (getButtonActionId() != ACTION_ID_HIDDEN) {
            val enable = Preferences.hasAllSerialNumbers(activity)
            findButtonActionById(getButtonActionId()).isEnabled = enable
            notifyButtonActionChanged(findButtonActionPositionById(getButtonActionId()))
        }
    }

    override fun onGuidedActionClicked(action: GuidedAction?) {
        if (Preferences.getDeviceCountRange(activity).contains(action?.id)) {
            val serialNumberId = action!!.id
            val serialNumber = parseLong(action.editTitle.toString())
            if (getButtonActionId() == ACTION_ID_HIDDEN) {
                // Launch web activity with given id and keep hosting activity in background.
                launchWebActivity(activity, serialNumberId)
                activity?.finishAfterTransition()
            } else {
                Preferences.setSerialNumber(activity, serialNumberId, parseLong("$serialNumber"))
                updateButtonAction()
            }
        } else if (action?.id == getButtonActionId()) {
            if (onDoConfirmSettings(action.id)) {
                Preferences.markSettingsConfirmed(activity)
            }
            // Launch the device chooser fragment
            add(fragmentManager, SelectDeviceGuidedStep())
        } else {
            fragmentManager?.popBackStack()
        }
    }
}