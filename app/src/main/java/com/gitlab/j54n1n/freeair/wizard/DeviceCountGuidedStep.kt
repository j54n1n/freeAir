package com.gitlab.j54n1n.freeair.wizard

import android.os.Bundle
import androidx.leanback.app.GuidedStepSupportFragment
import androidx.leanback.widget.GuidanceStylist
import androidx.leanback.widget.GuidedAction
import com.gitlab.j54n1n.freeair.R
import com.gitlab.j54n1n.freeair.wizard.sn.EnterSerialNumbersGuidedStep


class DeviceCountGuidedStep : GuidedStepSupportFragment() {

    companion object {
        private const val ACTION_ID_DEVICE_COUNT = 1L
    }

    override fun onCreateGuidance(savedInstanceState: Bundle?): GuidanceStylist.Guidance {
        val title = activity?.getString(R.string.app_name)
        val description = activity?.getString(R.string.wizard_device_count_desc)
        val breadcrumb = activity?.getString(R.string.wizard_breadcrumb_initial_step)
        return GuidanceStylist.Guidance(title, description, breadcrumb, null)
    }

    override fun onCreateActions(actions: MutableList<GuidedAction>, savedInstanceState: Bundle?) {
        val title = activity?.getString(R.string.wizard_action_device_count_title)
        val description = resources.getQuantityString(
            R.plurals.wizard_action_device_count, 0, 0)
        val subActions: MutableList<GuidedAction> = ArrayList()
        actions.add(GuidedAction.Builder(activity)
            .id(ACTION_ID_DEVICE_COUNT)
            .title(title)
            .editTitle("")
            .description(description)
            .subActions(subActions)
            .build()
        )
    }

    override fun onCreateButtonActions(
        actions: MutableList<GuidedAction>, savedInstanceState: Bundle?
    ) {
        actions.add(GuidedAction.Builder(activity)
            .clickAction(GuidedAction.ACTION_ID_CONTINUE)
            .build()
        )
        actions[actions.size - 1].isEnabled = false
    }

    override fun onResume() {
        super.onResume()
        val deviceCountAction = findActionById(ACTION_ID_DEVICE_COUNT)
        val deviceCountSubActions = deviceCountAction.subActions
        deviceCountSubActions.clear()
        for (i in Preferences.getDeviceCountMaxRange()) {
            val title = resources.getQuantityString(R.plurals.wizard_action_device_count, i, i)
            deviceCountSubActions.add(GuidedAction.Builder(activity)
                .id(i.toLong())
                .title(title)
                .description("")
                .checkSetId(GuidedAction.DEFAULT_CHECK_SET_ID)
                .checked(i.toLong() == Preferences.getDeviceCount(activity))
                .build()
            )
        }
    }

    override fun onSubGuidedActionClicked(action: GuidedAction?): Boolean {
        if (action!!.isChecked) {
            findActionById(ACTION_ID_DEVICE_COUNT).description = action.title
            notifyActionChanged(findActionPositionById(ACTION_ID_DEVICE_COUNT))
            findButtonActionById(GuidedAction.ACTION_ID_CONTINUE).isEnabled = true
            notifyButtonActionChanged(findButtonActionPositionById(GuidedAction.ACTION_ID_CONTINUE))
            return true
        }
        return false
    }

    override fun onGuidedActionClicked(action: GuidedAction?) {
        if (action?.id == GuidedAction.ACTION_ID_CONTINUE) {
            val deviceCountSubActions = findActionById(ACTION_ID_DEVICE_COUNT).subActions
            val deviceCountSubAction = deviceCountSubActions.firstOrNull { it.isChecked }
            val deviceCount = deviceCountSubAction?.id
            if (deviceCount != null) {
                Preferences.setDeviceCount(activity, deviceCount)
            }
            //TODO: Change fragment
            val fragment = EnterSerialNumbersGuidedStep()
            add(fragmentManager, fragment)
        } else {
            fragmentManager?.popBackStack()
        }
    }
}