package com.gitlab.j54n1n.freeair

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_web.*

/**
 * Activity hosting the web application with given bluMartin FreeAir device serial number.
 */
class WebActivity : Activity() {

    companion object {

        /**
         * Put given serial number on its launch intent.
         * @param intent the given launch intent.
         * @param serialNumber the device serial number.
         */
        fun putDeviceSN(intent: Intent, serialNumber: Long) {
            intent.putExtra(PARAM_DEVICE_SN, serialNumber)
        }

        private fun getDeviceSN(intent: Intent): Long {
            return intent.getLongExtra(PARAM_DEVICE_SN, PARAM_DEVICE_SN_INVALID)
        }

        private val PARAM_DEVICE_SN = WebActivity::class.java.name + "PARAM_DEVICE_SN"
        private const val PARAM_DEVICE_SN_INVALID = -1L

        private val WEB_APP_URLS = arrayOf("https://freeair-connect.de", "https://blumartin.de")
    }

    private lateinit var webHandler: WebHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        webHandler = FreeAirWebHandler()
        webView.loadUrl(getBaseUrl())
    }

    private fun getBaseUrl() : String {
        val serialNumber = getDeviceSN(intent)
        if (serialNumber != PARAM_DEVICE_SN_INVALID) {
            return "${WEB_APP_URLS[0]}/?sn=$serialNumber"
        }
        return "${WEB_APP_URLS[0]}/"
    }

    private inner class FreeAirWebHandler() : WebHandler(webView, progressBar, reloadButton) {

        override fun abortUrlLoading(url: String): Boolean {
            val doAbort = !(url.startsWith(WEB_APP_URLS[0])
                    || url.startsWith(WEB_APP_URLS[1]))
            if (doAbort) {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.warning_denied_loading_url, url),
                    Toast.LENGTH_LONG
                ).show()
            }
            return doAbort
        }

        override fun failedToLoad(errorCode: Int?, description: String?, failingUrl: String?) {
            tryAllowReload(failingUrl, getBaseUrl())
            Toast.makeText(
                applicationContext,
                getString(R.string.error_failed_loading_url, failingUrl),
                Toast.LENGTH_LONG
            ).show()
        }

        private fun itemFocusGainedColor(): String {
            // Convert Android ARGB color and format it as RGB HTML color.
            val argb = ContextCompat.getColor(applicationContext, R.color.colorFocusBackground)
            return "#%02X%02X%02X".format(Color.red(argb), Color.green(argb), Color.blue(argb))
        }

        override fun onPageLoadedExecuteJavaScript(): String {
            val focusInColor = itemFocusGainedColor()
            return """
                document.addEventListener('focusin', function(ev) {
                    ev.target.style.backgroundColor = '$focusInColor';
                });
                document.addEventListener('focusout', function(ev) {
                    ev.target.style.backgroundColor = '';
                });
                document.getElementById('sn').disabled = true;
            """.trimIndent()
        }
    }

    fun onReloadButton(view: View) {
        webHandler.reload(getBaseUrl())
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        val isGoBackDone = webHandler.tryGoBack(keyCode)
        return if (isGoBackDone) true else super.onKeyDown(keyCode, event)
    }
}