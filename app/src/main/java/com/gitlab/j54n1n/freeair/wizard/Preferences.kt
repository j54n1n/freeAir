package com.gitlab.j54n1n.freeair.wizard

import android.app.Activity
import android.content.Context

object Preferences {

    /** Set the maximum permissible number of devices. */
    const val MAX_DEVICE_COUNT = 4

    /**
     * Set or update the number of devices to be assigned a serial number.
     * @param activity the hosting activity of the guided steps.
     * @param newDeviceCount the number of devices to be considered.
     */
    fun setDeviceCount(activity: Activity?, newDeviceCount: Long) {
        put(activity, PREF_KEY_DEVICE_COUNT, newDeviceCount)
    }

    /**
     * Determine if the number of devices has been chosen.
     * @param activity the hosting activity of the guided steps.
     * @return true if chosen else false.
     */
    fun hasDeviceCount(activity: Activity?): Boolean {
        return (getDeviceCount(activity) != PREF_LONG_INVALID)
    }

    /**
     * Retrieve the count of devices that has been chosen.
     * @param activity the hosting activity of the guided steps.
     * @return the number of devices.
     */
    fun getDeviceCount(activity: Activity?): Long {
        return get(activity, PREF_KEY_DEVICE_COUNT)
    }

    /**
     * Retrieve the range of permissible actions ids after the number of devices has been chosen.
     * @param activity the hosting activity of the guided steps.
     * @return a range from 1 till to a maximum number (inclusive).
     */
    fun getDeviceCountRange(activity: Activity?): LongRange {
        return 1L..getDeviceCount(activity)
    }

    /**
     * Retrieve the maximum range of allowed action id.
     * @return a range from 1 till to a maximum number (inclusive).
     */
    fun getDeviceCountMaxRange(): IntRange {
        return 1..MAX_DEVICE_COUNT
    }

    /**
     * Save given serial number by its action id.
     * @param activity the hosting activity of the guided steps.
     * @param id the action id for which the serial number is set.
     */
    fun setSerialNumber(activity: Activity?, id: Long, newSerialNumber: Long) {
        put(activity, "${PREF_KEY_SERIAL_NUMBER}_$id", newSerialNumber)
    }

    /**
     * Retrieve if the given serial number inserted from a guided step has been saved.
     * One more guided step may be necessary to confirm the inserted serial number.
     * @param activity the hosting activity of the guided steps.
     * @param id the action id for which the serial number is being requested.
     * @return true when a serial number has been saved else false.
     */
    fun hasSerialNumber(activity: Activity?, id: Long): Boolean {
        return (getSerialNumber(activity, id) != PREF_LONG_INVALID)
    }

    /**
     * Retrieve if all the given serial numbers inserted from a guided step has been saved.
     * One more guided step may be necessary to confirm the inserted serial numbers.
     * @param activity the hosting activity of the guided steps.
     * @return true when a serial number has been saved else false.
     */
    fun hasAllSerialNumbers(activity: Activity?): Boolean {
        for (id in 1L..getDeviceCount(activity)) {
            if (!hasSerialNumber(activity, id)) {
                return false
            }
        }
        return true
    }

    /**
     * Retrieve given serial number by its action id.
     * @param activity the hosting activity of the guided steps.
     * @param id the action id for which the serial number is being requested.
     * @return the serial number if it has been saved else -1.
     */
    fun getSerialNumber(activity: Activity?, id: Long): Long {
        return get(activity, "${PREF_KEY_SERIAL_NUMBER}_$id")
    }

    /**
     * Retrieve if the saved serial numbers has been confirmed by a guided step.
     * Further guided steps may be not necessary anymore and therefore can be omitted.
     * @param activity the hosting activity of the guided steps.
     * @return true when a saved serial number has been confirmed else false.
     */
    fun hasConfirmedSettings(activity: Activity?): Boolean {
        return (get(activity, PREF_KEY_CONFIRM_SETTINGS) != PREF_LONG_INVALID)
    }

    /**
     * Mark the current settings as definite and mark setup as finished.
     * @param activity the hosting activity of the guided steps.
     */
    fun markSettingsConfirmed(activity: Activity?) {
        put(activity, PREF_KEY_CONFIRM_SETTINGS, PREF_LONG_TRUE)
    }

    private val PREF_KEY_DEVICE_COUNT = Preferences::class.java.name + ".DEV_CNT"
    private val PREF_KEY_SERIAL_NUMBER = Preferences::class.java.name + ".SER_NUM"
    private val PREF_KEY_CONFIRM_SETTINGS = Preferences::class.java.name + ".CONFIRM"

    private const val PREF_LONG_INVALID = -1L
    private const val PREF_LONG_TRUE = 1L

    private fun put(activity: Activity?, key: String?, newValue: Long) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putLong(key, newValue)
            commit()
        }
    }

    private fun get(activity: Activity?, key: String?): Long {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return PREF_LONG_INVALID
        return sharedPref.getLong(key, PREF_LONG_INVALID)
    }
}